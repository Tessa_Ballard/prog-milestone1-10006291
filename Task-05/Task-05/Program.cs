﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05
{
    class Program
    {
        static void Main(string[] args)
        {
            int UserTime;
            Console.Write("Please enter a time: ");
            UserTime = int.Parse(Console.ReadLine());
            if (UserTime > 24 || UserTime < 0)
            {
                Console.WriteLine("Invalid Input");
            }
            else if (UserTime > 12)
            {
                Console.WriteLine("The time is " + (UserTime - 12) + " pm.");
            }
            else
            {
                Console.WriteLine("The time is " + UserTime + " am.");
            }           

            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }
    }
}
