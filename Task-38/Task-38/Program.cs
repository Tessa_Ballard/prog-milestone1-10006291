﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_38
{
    class Program
    {
        static void Main(string[] args)
        {
            int UserTables;
            Console.WriteLine("Please enter a number to show division of times tables");
            UserTables = int.Parse(Console.ReadLine());

            for (int i = 1; i < 13; i++)
            {
                int result = i / UserTables;
                Console.WriteLine(i + " / " + UserTables + " = " + result);
            }

            Console.WriteLine("Press enter to continue");
            Console.ReadLine();
            {

            }
        }
    }
}
