﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string, int>>();
            names.Add(Tuple.Create("Jess", 19));
            names.Add(Tuple.Create("Hayley", 25));
            names.Add(Tuple.Create("Mavis", 21));

            names[0].Item1  //"Jess"
            names[0].Item2 //19
            
            names[0].Item1 //"Hayley"
            names[0].Item2 //25
        }
    }
}
