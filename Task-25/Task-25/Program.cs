﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_25
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number then press enter");

            var input = Console.ReadLine();
            var a = 0;
            bool value = int.TryParse(input, out a);

            Console.WriteLine($"You typed in: {input}");
            Console.WriteLine($"Are you sure you typed in a number? : {value}");

            Console.ReadLine();
        }
    }
}
