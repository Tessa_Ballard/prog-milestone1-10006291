﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a year number");
            int UserYear;
            UserYear = int.Parse(Console.ReadLine());
            if(DateTime.IsLeapYear(UserYear))
            {
                Console.WriteLine(UserYear + " is a leap year");
            }
            else
            {
                Console.WriteLine(UserYear + " is not a leap year");
            }

            Console.WriteLine("Press enter to continue");
            Console.ReadLine();
        }
    }
}
