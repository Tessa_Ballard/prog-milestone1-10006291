﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_15
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = 0;
            while (true)
            {
                Console.WriteLine("Please enter 5 numbers separated by a space: ");
                string numbers;
                numbers = Console.ReadLine();
                string[] myArray = numbers.Split(' ');
                int length = myArray.Length;
                if (length == 5)
                {
                    foreach (var item in myArray)
                    {
                        int i = int.Parse(item);
                        result = result + i;
                    }
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid Input!");
                    numbers = string.Empty;
                }
            }           
            
            Console.WriteLine("The result is: " + result);
            Console.ReadLine();
           
            
        }
    }
}
