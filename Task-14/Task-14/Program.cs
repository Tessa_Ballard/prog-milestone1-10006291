﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number to calculate computer storage space");
            var input = double.Parse(Console.ReadLine());

            const double Tb2Gb = 1024;

            var answer = Math.Round(input * Tb2Gb, 2);

            Console.WriteLine($" {input} Tb is {answer} Gigabytes ");

            Console.ReadLine();
        }
    }
}
