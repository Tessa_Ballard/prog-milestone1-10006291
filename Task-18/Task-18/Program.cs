﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<string> { "Jess", "Hayley", "Kayla" };
            var month = new List<string> { "June", "September", "August" };
            var birthday = new List<string> { "26", "15", "10" };
            names.Add("Georgia");
            names.Add("Jacqueline");
            names.Add("Sophie");

            Console.WriteLine(string.Join(",", names));
            Console.WriteLine(string.Join(",", month));
            Console.WriteLine(string.Join(",", birthday));

            Console.ReadLine();
        }
    }
}
