﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_40
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime startDate = new DateTime(2016, 8, 1);
            DateTime endDate = new DateTime(2016, 8, 31);

            TimeSpan diff = endDate - startDate;
            int days = diff.Days;
            for (var i = 0; i <= days; i++)
            {
                var testDate = startDate.AddDays(i);
                switch (testDate.Date.DayOfWeek)
                {

                    case DayOfWeek.Wednesday:
                        Console.WriteLine(testDate.ToShortDateString());
                        break;

                }
            }
            Console.WriteLine("The number of Wednesdays in a month eg August 2016 is 5");
            Console.ReadLine();
        }
    }
}
