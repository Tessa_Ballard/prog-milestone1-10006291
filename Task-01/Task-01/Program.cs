﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            string myName, input;
            int age;
            Console.WriteLine("What is your name?");
            myName = Console.ReadLine();
            Console.WriteLine("What is your age?");
            input = Console.ReadLine();
            age = Convert.ToInt32(input);
            Console.WriteLine("My name is " + myName, " and I am " + age + " years old.");
            Console.WriteLine("My name is {0} and I am {1} years old.",myName, age);
            Console.WriteLine($"My name is {myName} and I am {age} years old.");
            Console.ReadLine();
        }
    }
}
