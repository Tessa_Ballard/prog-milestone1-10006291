﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_07
{
    class Program
    {
        static void Main(string[] args)
        {
            int UserTimesTables;
            Console.WriteLine("Please enter a number to show times tables");
            UserTimesTables = int.Parse(Console.ReadLine());

            for (int i = 1; i < 13; i++)
            {
                int result = i * UserTimesTables;
                Console.WriteLine(i + " * " + UserTimesTables + " = " + result );                
            }

            Console.WriteLine("Press enter to continue");
            Console.ReadLine();
        }
    }
}
