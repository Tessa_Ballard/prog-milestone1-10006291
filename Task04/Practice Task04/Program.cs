﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice_Task04
{
    class Program
    {
        static void Main(string[] args)
        {
            int UserInput;
            Console.WriteLine("To convert Celsius to Fahrenheit press 1"); 
            Console.WriteLine("To convert Fahrenheit to Celsius press 2"); 
            UserInput = int.Parse(Console.ReadLine());           //Read the user input
            switch(UserInput)
            {
                case 1:
                    Console.WriteLine("Please enter a number to convert celcius to fahrenheit");

                    double celsius = double.Parse(Console.ReadLine());

                    double Fahrenheit = ((celsius * 9) / 5) + 32;

                    Console.WriteLine("Fahrenheit:" + Fahrenheit);
                    break;

                case 2:
                    Console.WriteLine("Please enter a number to convert fahrenheit to celcius");
                    double fahrenheit = double.Parse(Console.ReadLine());

                    celsius = ((fahrenheit - 32) / 9) * 5;

                    Console.WriteLine("Celcius :" + celsius);
                    break;

                default:
                    Console.WriteLine("Invalid input");
                    break;
            }
            
            
            

            
            Console.ReadLine();
        }
    }
}
