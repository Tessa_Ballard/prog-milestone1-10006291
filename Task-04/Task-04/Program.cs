﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input the temperature value from Celcius to Fahrenheit");
            int cel = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int CtoF = ((cel * 9) / 5) + 32;
            Console.WriteLine("The Farenheit value is {0}: ", CtoF);
            Console.ReadLine();

            Console.WriteLine("Input the temperature value from Fahrenheit to Celcius");
            int fah = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int FtoC = (fah - 32) / 9) *5;
            Console.WriteLine("The Ceclius value is {0}", FtoC);
            Console.ReadLine();

        }
    }
}
