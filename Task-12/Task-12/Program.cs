﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number.");
            int UserNumber;
            UserNumber = int.Parse(Console.ReadLine());
            if(UserNumber % 2 == 1)
            {
                Console.WriteLine(UserNumber + " is an odd number");
            }
            else
            {
                Console.WriteLine(UserNumber + " is an even number");
            }
            Console.WriteLine("Press enter to continue");
            Console.ReadLine();          
            
        }
    }
}
