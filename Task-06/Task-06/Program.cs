﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_06
{
    class Program
    {
        static void Main(string[] args)
        {
            int UserNumber;
            Console.Write("Please enter a number: ");
            UserNumber = int.Parse(Console.ReadLine());
            int result = 0;

            for (int i = UserNumber; i < UserNumber + 5; i++)
            {
                result = result + i;                    
            }

            Console.WriteLine("The result is "+result);
            Console.WriteLine("Press enter to continue");
            Console.ReadLine();
               
        }
    }
}
