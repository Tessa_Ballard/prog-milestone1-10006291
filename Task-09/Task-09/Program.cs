﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_09
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 2016; i <=2036; i++)
            {
                if (DateTime.IsLeapYear(i))
                {
                    Console.WriteLine("Year " + i + " is a leap year.");
                }
            }
            Console.ReadLine();
        }
    }
}
