﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_20
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> oddNum = new List<int>();
            int[] Array = new int[] { 33, 45, 44, 67, 87, 86 };

            foreach (int item in Array)
            {
                if (item % 2 != 0)
                {
                    oddNum.Add(item);
                }
            }
            

            Console.WriteLine("Odd numbers : ");
            foreach (var item in oddNum)
            {
                Console.WriteLine(item + "\n");
            }

            Console.ReadLine();
        }
    }
}
